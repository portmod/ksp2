# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import fnmatch
import os
import re
from typing import Callable, Iterable, List, Optional, Pattern, Set, Tuple, Union, cast

from pybuild import Pybuild1, patch_dir


class Package(Pybuild1):
    NAME = "CKAN"
    DESC = "Pybuild Class for packages converted from CKAN"
    KEYWORDS = "ksp2"


# Wrapper to hide the fact that this is broken on the latest release
def check_required_use(*args, **kwargs):
    from pybuild import check_required_use

    return check_required_use(*args, **kwargs)


def list_to_re(patterns: List[str]) -> Pattern:
    def to_re(value: Union[str, Pattern]):
        """
        Converts fn-match string into a regular expression string

        Note that normpath may not work as expected with fn-match strings
        if forward-slashes are present inside bracketed ranges (e.g. [/../]).
        """
        if isinstance(value, Pattern):
            return value
        return fnmatch.translate(os.path.normpath(value))

    flags = 0
    if os.environ.get("CASE_INSENSITIVE_FILES", False):
        flags = re.IGNORECASE
    return re.compile("|".join(map(to_re, patterns)), flags=flags)


class Install:
    """
    Represents a file or directory to be installed

    Note that arbitrary arguments can be passed to the constructor, as
    repositories may make use of custom information.
    See the repository-level documentation for such information.
    """

    def __init__(
        self,
        path: str,
        required_use: str = "",
        S: Optional[str] = None,
        include_only: Optional[List[str]] = None,
        include_only_regexp: Optional[List[str]] = None,
        filter: Optional[List[str]] = None,
        filter_regexp: Optional[List[str]] = None,
        install_to: Optional[str] = None,
        rename: Optional[str] = None,
        doc: Iterable[str] = (),
        find: bool = False,
        find_regexp: bool = False,
        find_matches_files: bool = False,
    ):
        self.path: str = path
        """
        The path to the data directory that this Install represents
        relative to the root of the archive it is contained within.
        """
        self.required_use: str = required_use
        """
        A list of use flags with the same format as the package's
        REQUIRED_USE variable which enable the Install if satisfied.
        Defaults to an empty string that is always satisfied.
        """
        self.S: Optional[str] = S
        """
        The source directory corresponding to this Install.

        Similar function to S for the entire pybuild, this determines which directory
        contains this Install, and generally corresponds to the name of the source
        archive, minus extensions. This is required for packages that contain more
        than one source, but is automatically detected for those with only one source
        if it is not specified, and will first take the value of Pybuild1.S, then the
        source's file name without extension if the former was not defined.
        """
        self.include_only: Optional[List[str]] = include_only
        """
        If present, only installs files matching the patterns in this list.
        fnmatch-style globbing patterns (e.g. * and [a-z]) can be used
        """
        self.include_only_regexp: Optional[List[str]] = include_only
        """
        If present, only installs files matching the regexp patterns in this list.
        """
        self.filter: Optional[List[str]] = filter
        """
        If present, does not install files matching the patterns in this list.
        fnmatch-style globbing patterns (e.g. * and [a-z]) can be used

        Note: should match against file and directory names, not their paths
        """
        self.filter_regexp: Optional[List[str]] = filter_regexp
        """
        If present, does not install files matching the regex patterns in this list.
        """
        self.install_to: Optional[str] = install_to
        """
        Destination path for the entity at path, which will be moved inside this directory
        """
        self.rename: Optional[str] = rename
        """
        Destination name of this entry within the final directory.

        E.g.::

            Install("foo/bar", rename="baz")

        Will install the contents of ``foo/bar`` (in the source) into the directory
        ``foo/baz`` inside the package's installation directory (and also the VFS).
        """
        self.doc: List[str] = list(doc)
        """
        A list of patterns matching documentation files within the package

        This documentation will be installed separately
        fnmatch-style globbing patterns (e.g. * and [a-z]) can be used.
        """
        self.find: bool = find
        """
        If true, the path specified is not precise and the path used will be the
        top-most directory which exactly matches the name specified
        """
        self.find_regexp: bool = find_regexp
        """
        If true, the path specified is treated as a regexp and the path used will be the
        top-most directory which matches the regexp
        """
        self.find_matches_files: bool = find_matches_files
        """
        If True, find will match files as well as directories
        """


class CKAN(Pybuild1):
    """
    Pybuild Class for packages converted from CKAN

    INSTALL_DIRS has been replaced with INSTALL, and now works for installing
    both directories and individual files
    """

    INSTALL: List[Install]

    def install(self, source: str, install: Install, to_install: Set[str]):
        case_insensitive = os.environ.get("CASE_INSENSITIVE_FILES", False)

        if install.install_to is None and install.rename is None:
            dest = self.D
        else:
            dest = os.path.normpath(
                os.path.join(
                    self.D,
                    install.install_to or ".",
                    install.rename or os.path.basename(source),
                )
            )

        filter_entries = install.filter or []
        if isinstance(filter_entries, str):
            filter_entries = [filter_entries]

        filter_re = list_to_re(filter_entries)
        if install.filter_regexp:
            filter_re = list_to_re([filter_re.pattern, install.filter_regexp])
        if install.include_only is None:
            include_only = None
        else:
            include_only = list_to_re(install.include_only)

        if install.include_only_regexp:
            if include_only:
                include_only = list_to_re(
                    [include_only.pattern, install.include_only_regexp]
                )
            else:
                include_only = install.include_only_regexp

        def get_listfn(filter_re: Pattern, polarity: bool):
            def fn(directory: str, contents: Iterable[str]):
                if polarity:
                    return {
                        file
                        for file in contents
                        if filter_re.match(file)
                        and not os.path.isdir(os.path.join(directory, file))
                    }
                else:
                    return {
                        file
                        for file in contents
                        if not filter_re.match(file)
                        and not os.path.isdir(os.path.join(directory, file))
                    }

            return fn

        # Function to ingore additional paths, given an existing ignore function
        def ignore_more(
            ignorefn, to_ignore: List[str]
        ) -> Callable[[str, List[str]], Set[str]]:
            def fn(directory: str, contents: Iterable[str]):
                results = ignorefn(directory, contents)
                for name in contents:
                    if any(
                        os.path.normpath(os.path.join(directory, name))
                        == os.path.normpath(path)
                        for path in to_ignore
                    ):
                        results.add(name)
                return results

            return fn

        # Determine if any other Installs are inside this one and add them to the blacklist
        to_ignore = []
        for other_path in to_install:
            if other_path != source and os.path.commonpath(
                [source]
            ) == os.path.commonpath(
                [os.path.abspath(source), os.path.abspath(other_path)]
            ):
                to_ignore.append(other_path)

        os.makedirs(os.path.dirname(dest), exist_ok=True)

        if os.path.islink(source):
            linkto = os.readlink(source)
            if os.path.exists(dest):
                os.rmdir(dest)
            os.symlink(linkto, dest, True)
        elif os.path.isfile(source):
            os.rename(source, dest)
        elif include_only is not None:
            ignore = get_listfn(include_only, False)
            if to_ignore:
                ignore = ignore_more(ignore, to_ignore)
            patch_dir(source, dest, ignore=ignore, case_sensitive=not case_insensitive)
        elif filter_entries:
            ignore = get_listfn(filter_re, True)
            if to_ignore:
                ignore = ignore_more(ignore, to_ignore)
            patch_dir(source, dest, ignore=ignore, case_sensitive=not case_insensitive)
        else:
            ignore = None
            if to_ignore:
                ignore = ignore_more(lambda d, c: set(), to_ignore)
            patch_dir(source, dest, case_sensitive=not case_insensitive, ignore=ignore)

    def src_install(self):
        to_install: List[Tuple[str, Install, bool]] = []
        sources = set()
        for install in self.INSTALL:
            source_dir = install.S or cast(str, self.S)
            source = None
            if install.find or install.find_regexp:

                def bf_walk(path):
                    entries = []
                    for dirpath, dirnames, filenames in os.walk(path):
                        for entry in dirnames:
                            path = os.path.join(dirpath, entry)
                            entries.append((path.count(os.path.sep), path))

                        if install.find_matches_files:
                            for entry in filenames:
                                path = os.path.join(dirpath, entry)
                                entries.append((path.count(os.path.sep), path))

                    for entry in sorted(entries):
                        yield entry[1]

                flags = 0
                if os.environ.get("CASE_INSENSITIVE_FILES", False):
                    flags = re.IGNORECASE

                # BFS to find install.path
                for path in bf_walk(os.path.join(self.WORKDIR, source_dir)):
                    # From Spec:
                    #   Directories separators will have been normalised to forward-
                    #   slashes first, and the trailing slash for each directory
                    #   removed before the regular expression is run
                    normalized = path.replace("\\", "/").rstrip("/")
                    # Note: It's not clear from the spec if find can be used when
                    #       path contains path separators, however there are instances
                    #       of this in the repository. I can only assume that directories
                    #       should be normalized in the same manner as for find_regexp
                    if install.find and re.search(
                        install.path + "$", normalized, flags=flags
                    ):
                        source = os.path.normpath(path)
                        break
                    if install.find_regexp and re.search(
                        install.path, normalized, flags=flags
                    ):
                        source = os.path.normpath(path)
                        break

                if source is None:
                    raise Exception(
                        f"Unable to find Install source matching {install.path}"
                    )
            else:
                source = os.path.normpath(
                    os.path.join(self.WORKDIR, source_dir, install.path)
                )
            sources.add(source)
            if check_required_use(install.required_use, self.get_use(), self.valid_use):
                # self.S will be set in package.py via the PhaseState
                to_install.append((source, install, True))
            else:
                to_install.append((source, install, False))

        for source, install, enabled in to_install:
            source_dir_path = os.path.join(install.S or cast(str, self.S), install.path)
            if enabled:
                dest = os.path.join(
                    self.ROOT,
                    install.install_to or ".",
                    install.rename or os.path.basename(source),
                )
                print(
                    # FIXME: repo-side localization
                    # l10n(
                    #    "installing-directory-into",
                    #    dir=magenta(source_dir_path),
                    #    dest=magenta(os.path.join(self.ROOT, install.rename)),
                    # )
                    f"Installing {source_dir_path} into {dest}"
                )
                for doc_path in set(install.doc) | {
                    "README*",
                    "ChangeLog",
                    "CHANGELOG*",
                    "AUTHORS*",
                    "NEWS*",
                    "TODO*",
                    "CHANGES*",
                    "THANKS*",
                    "BUGS*",
                    "FAQ*",
                    "CREDITS*",
                    "LICENSE*",
                    "ReadMe*",
                    "Doc/*",
                    "doc/*",
                    "docs/*",
                    "Docs/*",
                }:
                    self.dodoc(os.path.join(self.WORKDIR, source_dir_path, doc_path))
                self.install(source, install, sources)
            else:
                print(
                    # FIXME: repo-side localization
                    # l10n(
                    #    "skipping-directory",
                    #    dir=magenta(source_dir_path),
                    #    req=blue(install.required_use),
                    # )
                    f"Skipping {source_dir_path} due to unsatisfied use requirements {install.required_use}"
                )
