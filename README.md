# The KSP2 Mod Package Repository

This repository contains packages that can be used by Portmod to install mods for KSP2.

See [Portmod](https://gitlab.com/portmod/portmod/) for details on how to use this repository to install mod packages.

If you are interested in contributing packages, see [the OpenMW-mods CONTRIBUTING page](https://gitlab.com/portmod/openmw-mods/-/blob/master/CONTRIBUTING.md) (no custom one here yet).
